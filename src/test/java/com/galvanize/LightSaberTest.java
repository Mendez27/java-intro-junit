package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LightSaberTest {
    @Test
    public void verifyThatSerialNumberWillReturn() {
        //Setup
        LightSaber trial = new LightSaber(9);
        //Execute
        long test = trial.getJediSerialNumber();
        //Assert
        assertEquals(9, test);
    }

    @Test
    public void verifyThatSetChargeWillReturnAccurateOnceCalled() {
        //Setup
        LightSaber trial = new LightSaber(5);
        //Execute
        trial.setCharge(50.0f);
        float bump = trial.getCharge();
        //Assert
        assertEquals(50.0f, bump);
    }

    @Test
    public void verifyThatColorWillBeRetrieved() {
        //Setup
        LightSaber trial = new LightSaber(5);
        //Execute
        trial.setColor("Blue");
        String bump = trial.getColor();
        //Assert
        assertEquals("Blue", bump);
    }

    @Test
    public void verifyUsageWillReflect() {
        LightSaber trial = new LightSaber(6);
        trial.use(1.0f);
        float bluff = trial.getCharge();
        assertEquals(99.833336f, bluff);
    }

    @Test
    public void verifyMinutesWillReflect() {
        LightSaber trial = new LightSaber(6);
        trial.use(1.0f);
        float bluff = trial.getRemainingMinutes();
        assertEquals(299.5, bluff);
    }

    @Test
    public void verifyRechargeWillReflect() {
        LightSaber trial = new LightSaber(6);
        trial.use(9.0f);
        trial.recharge();
        float bluff = trial.getCharge();
        assertEquals(100.0f, bluff);
    }

    @Test
    public void verifyTestCount() {
        LightSaber trial = new LightSaber(5);
        boolean test = true;
        assertTrue(test);
    }

    @Test
    public void verifyThatSerialNumberWillReturnAgain() {
        //Setup
        LightSaber trial = new LightSaber(6);
        //Execute
        long test = trial.getJediSerialNumber();
        //Assert
        assertEquals(6, test);
    }

    //In total, this project wanted 8 complete test that would call upon
    //the methods in our provided LightSaber class. After pulling each
    //method, it seems all we had to do was compile and additional layer
    //of tests.
}
